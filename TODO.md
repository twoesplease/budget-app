 ### Changes to implement in future versions of Budget App
  
##### Improve my testing skills
  - [ ] Add unit tests (successful budget creation/validation, successful transaction creation/validation, correct calculation of remaining amount)

##### Improve UX
  - [ ] Add reset password functionality
  - [ ] Add log out button 
  - [ ] Add algorithm to allow users to calculate how long it will take them to reach their goal & when they will reach it if they save $x per week/month 
  - [ ] Add ability to edit budget details
  - [ ] Add ability to edit transaction details

##### Practice styling (so I don't forget everything front-endy I've ever learned)
  - [ ] Add header
  - [ ] Sort budgets & transactions info into table


